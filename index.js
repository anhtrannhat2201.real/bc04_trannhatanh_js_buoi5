//hàm bài 1
function checkDiem(e) {
  return !(e <= 0);
}
function quanLyTuyenSinh() {
  // console.log("yes");
  var diemChuan = document.getElementById("inputDiemChuan").value * 1;
  var chonKhuVuc = document.getElementById("chonKhuVuc").value * 1;
  var chonDoiTuong = document.getElementById("chonDoiTuong").value * 1;
  var diemMonThu1 = document.getElementById("diemMon-Thu1").value * 1;
  var diemMonThu2 = document.getElementById("diemMon-Thu2").value * 1;
  var diemMonThu3 = document.getElementById("diemMon-Thu3").value * 1;

  var dk = !0;
  var result = 0;

  if (
    (dk &=
      checkDiem(diemMonThu1) &&
      checkDiem(diemMonThu2) &&
      checkDiem(diemMonThu3))
  ) {
    result =
      diemMonThu1 + diemMonThu2 + diemMonThu3 + (chonKhuVuc + chonDoiTuong);
    // console.log('result: ', result);
    if (result >= diemChuan) {
      result = "Bạn đã đậu.Tổng điểm: " + result;
      //   console.log('result: ', result);
    } else {
      result = "Bạn đã rớt.Tổng điểm:" + result;
      //   console.log('result: ', result);
    }
  } else {
    result = "Bạn đã rớt. Do có điểm nhỏ hơn hoặc bằng 0";
    // console.log('result: ', result);
  }

  document.getElementById("txtResult").innerHTML = `${result}`;
}
//end
//hàm bài 2

function tinhTienDien() {
  var nhapHoTen = document.getElementById("inputName").value;
  //   console.log('nhapHoTen: ', nhapHoTen);
  var SoKw = document.getElementById("inputKW").value * 1;
  //   console.log('SoKw: ', SoKw);
  var result = 0;
  function tinhSoKW() {
    const kw50_dau = 500,
      kwtren50 = 650,
      kw100_ke = 850,
      kw150_ke = 1100,
      kw_conlai = 1300;

    if (0 < SoKw && SoKw <= 50) {
      result = SoKw * kw50_dau;
      //   console.log('result: ', result);
    } else if (SoKw > 50 && SoKw <= 100) {
      result = 50 * kw50_dau + (SoKw - 50) * kwtren50;
      //   console.log('result: ', result);
    } else if (SoKw > 100 && SoKw <= 200) {
      result = 50 * kw50_dau + 50 * kwtren50 + (SoKw - 100) * kw100_ke;
      //   console.log('result: ', result);
    } else if (SoKw > 200 && SoKw <= 350) {
      result =
        50 * kw50_dau +
        50 * kwtren50 +
        100 * kw100_ke +
        (SoKw - 200) * kw150_ke;
      // console.log('result: ', result);
    } else if (SoKw > 350) {
      result =
        50 * kw50_dau +
        50 * kwtren50 +
        100 * kw100_ke +
        150 * kw150_ke +
        (SoKw - 350) * kw_conlai;
    } else {
      alert("Số kw không hợp lệ! Vui lòng nhập lại");
      //   console.log("result: ", result);
    }
  }
  tinhSoKW(SoKw);
  result = new Intl.NumberFormat("vn-VN").format(result);
  //   out
  document.getElementById(
    "txtElecBill"
  ).innerHTML = `Họ tên: ${nhapHoTen} <br> Tiền điện: ${result} `;
}
//end
